package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func performRequest(r http.Handler, method, path string) *httptest.ResponseRecorder {

	req, _ := http.NewRequest(method, path, nil)

	w := httptest.NewRecorder()

	r.ServeHTTP(w, req)

	return w
}

func TestGetUsers(t *testing.T) {

	body := map[string][]map[string]interface{}{
		"data": {
			{"ID": float64(1), "Name": "Vageesha", "Email": "vageesha@zopsmart.com"},
			{"ID": float64(2), "Name": "Arun", "Email": "arunnasarain@gmail.com"},
			{"ID": float64(3), "Name": "Ajay", "Email": "ajay@gmail.com"},
			{"ID": float64(4), "Name": "Abhi", "Email": "abhi@gmail.com"},
			{"ID": float64(5), "Name": "Sakshi", "Email": "sakshi@gmail.com"},
		},
	}

	server := setup()

	w := performRequest(server, "GET", "/users")

	assert.Equal(t, http.StatusOK, w.Code)

	var response map[string][]map[string]interface{}

	err := json.Unmarshal([]byte(w.Body.String()), &response)
	_ = err

	value, exists := response["data"]

	fmt.Println(value)
	fmt.Println(exists)

	assert.Nil(t, err)
	assert.True(t, exists)
	assert.Equal(t, body["data"], response["data"], "not equal")

}

func TestOneUser(t *testing.T) {

	/*body := map[string][]map[string]interface{}{
		"data": {
			{"ID": float64(1), "Name": "Vageesha", "Email": "vageesha@zopsmart.com"},
			{"ID": float64(2), "Name": "Arun", "Email": "arunnasarain@gmail.com"},
			{"ID": float64(3), "Name": "Ajay", "Email": "ajay@gmail.com"},
			{"ID": float64(4), "Name": "Abhi", "Email": "abhi@gmail.com"},
			{"ID": float64(5), "Name": "Sakshi", "Email": "sakshi@gmail.com"},
		},
	}*/

	body := map[string]map[string]interface{}{
		"data": {"ID": float64(2), "Name": "Arun", "Email": "arunnasarain@gmail.com"},
	}

	server := setup()

	w := performRequest(server, "GET", "/users/2")

	assert.Equal(t, http.StatusOK, w.Code)

	var response map[string]map[string]interface{}

	err := json.Unmarshal([]byte(w.Body.String()), &response)
	_ = err

	value, exists := response["data"]

	fmt.Println(value)
	fmt.Println(exists)

	assert.Nil(t, err)
	assert.True(t, exists)
	assert.Equal(t, body["data"], response["data"], "not equal")

}
