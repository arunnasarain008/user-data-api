package models

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"sync"

	"github.com/jinzhu/gorm"
	//pq
	_ "github.com/lib/pq"
)

//UserDet holds the user data
type UserDet struct {
	ID        uint64 `gorm:"primary_key"`
	FirstName string
	LastName  string
	Email     string
	Phone     string
	Address   string
}

//Channel is the struct for channel
type Channel struct {
	Count int
	Total int
}

//Progress is
var Progress = make(chan Channel)

// DB is Reference to db
var DB *gorm.DB

//ConnectDB establishes connection
func ConnectDB() {

	db, err := gorm.Open("postgres", "host=localhost port=5432 user=postgres password=postgres dbname=users sslmode=disable")

	if err != nil {
		panic(err)
	}

	db.SingularTable(true)

	db.DropTableIfExists(&UserDet{})
	db.CreateTable(&UserDet{})

	DB = db
}

//AddUsers adds the user to DB
func AddUsers(filename string) {

	csvFile, _ := os.Open(filename)
	csvReader := csv.NewReader(csvFile)
	var users []UserDet
	count := -1
	for {
		row, err := csvReader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err.Error())
		}
		users = append(users, UserDet{
			FirstName: row[0],
			LastName:  row[1],
			Email:     row[2],
			Phone:     row[3],
			Address:   row[4],
		})
		count++
	}

	var wg sync.WaitGroup

	for _, user := range users[1:] {

		wg.Add(1)
		temp := Channel{Total: count}

		go func(user UserDet, progress chan Channel) {

			if err := DB.Create(&user).Error; err != nil {
				panic(err)
			}
			defer wg.Done()

			Progress <- temp
		}(user, Progress)
	}
	status(users)

	wg.Wait()
}

//Count stores the status
var Count = Channel{Count: 0, Total: 0}

func status(users []UserDet) {
	for _, w := range users[1:] {
		temp := <-Progress
		Count.Count++
		Count.Total = temp.Total
		fmt.Println(w, Count)
	}
}

////////////////////////////////////////////////////////////////////////

//GetUsers fetches all users
func GetUsers() ([]UserDet, error) {

	var users []UserDet

	err := DB.Find(&users).Error

	return users, err

}

//////////////////////////////////////////////////////////////////////////

//OneUser fetches details of one user
func OneUser(id string) (UserDet, error) {

	var user UserDet

	err := DB.Where("id =?", id).Find(&user).Error

	return user, err

}

///////////////////////////////////////////////////////////////////////////

//UpdateUser updates the fields of user corresponding to the userid
func UpdateUser(user UserDet, id string) error {

	i, _ := strconv.ParseUint(id, 10, 64)
	user.ID = i
	err := DB.Save(&user).Error

	return err
}

/////////////////////////////////////////////////////////////////////////////

//DeleteUser deletes the user corresponding to the userid
func DeleteUser(id string) error {

	var user UserDet

	i, _ := strconv.ParseUint(id, 10, 64)
	user.ID = i

	err := DB.Delete(&user).Error

	return err

}
