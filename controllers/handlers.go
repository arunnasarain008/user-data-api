package controllers

import (
	"fmt"
	"net/http"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/arunnasarain008/user-data-api/models"
)

//UploadFile uploads the file to server
//and stores the user in DB
func UploadFile(c *gin.Context) {

	form, err := c.MultipartForm()
	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("get form err: %s", err.Error()))
		return
	}
	files := form.File["files"]

	for _, file := range files {
		filename := filepath.Base(file.Filename)
		if err := c.SaveUploadedFile(file, filename); err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintf("upload file err: %s", err.Error()))
			return
		}
		models.AddUsers(filename)
	}

	c.String(http.StatusOK, "Uploaded successfully ")

}

//GetUsers fetches all users
func GetUsers(c *gin.Context) {

	users, err := models.GetUsers()

	if err != nil {
		c.JSON(http.StatusForbidden, gin.H{"error": err.Error()})
	}
	c.JSON(http.StatusOK, gin.H{
		"data": users,
	})

}

//OneUser fetches the user details of given id
func OneUser(c *gin.Context) {

	id := c.Param("id")

	user, err := models.OneUser(id)

	if err != nil {
		c.JSON(http.StatusForbidden, gin.H{"error": err.Error()})
	}
	c.JSON(http.StatusOK, gin.H{
		"data": user,
	})

}

//UpdateUser updates the fields of user corresponding to the userid
func UpdateUser(c *gin.Context) {

	id := c.Param("id")

	var user models.UserDet

	err := c.ShouldBindJSON(&user)
	if err != nil {
		c.JSON(http.StatusForbidden, gin.H{"error": err.Error()})
	}

	err = models.UpdateUser(user, id)
	if err != nil {
		c.JSON(http.StatusForbidden, gin.H{"error": err.Error()})
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Updated successfully",
	})
}

//DeleteUser deletes the user corresponding to the userid
func DeleteUser(c *gin.Context) {

	id := c.Param("id")

	err := models.DeleteUser(id)
	if err != nil {
		c.JSON(http.StatusForbidden, gin.H{"error": err.Error()})
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Deleted successfully",
	})
}

//GetStatus get the uploaded records
func GetStatus(c *gin.Context) {

	count := strconv.Itoa(models.Count.Count)
	total := strconv.Itoa(models.Count.Total)

	c.String(http.StatusOK, count+" of "+total+" uploaded ")

	if count == total && total != "0" {
		c.String(http.StatusOK, "\nAll Users Added")
	}

}
