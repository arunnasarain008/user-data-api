package main

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/arunnasarain008/user-data-api/controllers"
	"gitlab.com/arunnasarain008/user-data-api/models"
)

func main() {

	r := setup()
	models.ConnectDB()

	r.Run()

	defer models.DB.Close()

}

func setup() *gin.Engine {

	r := gin.Default()

	models.ConnectDB()

	r.Static("/userdet/", "./frontend")

	r.POST("/upload/", controllers.UploadFile)

	r.GET("/users/", controllers.GetUsers)
	r.GET("/users/:id/", controllers.OneUser)
	r.PUT("/update/:id", controllers.UpdateUser)

	//Sample JSON to check update

	/*{

	            "FirstName": "Arun W",
	            "LastName": "S",
	            "Email": "arun@exotel.in",
	            "Phone": "913333333333",
	            "Address": "YYY\nZZZZ"
	}*/

	r.DELETE("/users/:id", controllers.DeleteUser)

	r.GET("/status", controllers.GetStatus)

	return r
}
