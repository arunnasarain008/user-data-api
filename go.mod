module gitlab.com/arunnasarain008/user-data-api

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.16
	github.com/lib/pq v1.8.0
	github.com/stretchr/testify v1.6.1
	gitlab.com/arunnasarain008/api-users v0.0.0-20201014075113-6a25088c6830
)
